#include <Arduino.h>
#include <DebugUtils/DebugUtils.h>
#include "config.h"
#include "networking.h"
#include "servocontrol.h"

// PROTOTYPES

// !PROTOTYPES

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);
  DEBUGPRINTLN0("");
  setupNetwork();
  setupServo();
  DEBUGPRINTLN0("Setup finished - Jumping now into the loop");
}

void loop()
{
  // put your main code here, to run repeatedly:
}