#include <DebugUtils/DebugUtils.h>
#include <WiFiManager.h>

void setupNetwork()
{
  DEBUGPRINTLN0("[setupNetwork()]");
  WiFiManager wifiManager;

  // check if the saved configuration should be discarded
  pinMode(CONFIGPIN, INPUT);
  if (digitalRead(CONFIGPIN) == HIGH)
  {
    DEBUGPRINTLN1("[setupNetwork()] Going to delete the saved WiFi settings now");
    wifiManager.resetSettings();
  }

#if DEBUGLEVEL >= 3
  // activate debugoutput of WiFiManager
  wifiManager.setDebugOutput(true);
#endif

  // set custom ip for portal
  wifiManager.setAPStaticIPConfig(IPAddress(192, 168, 254, 1), IPAddress(192, 168, 254, 1), IPAddress(255, 255, 255, 0));
  // TODO: configure the client, if connected successful
#ifdef APNAME
#ifdef APPWD
  DEBUGPRINTLN1("[setupNetwork()] APNAME and APPWD is defined");
  wifiManager.autoConnect(APNAME, APPWD);
#else
  DEBUGPRINTLN1("[setupNetwork()] APNAME is defined but APPWD is not defined");
  wifiManager.autoConnect(APNAME);
#endif
#else
  DEBUGPRINTLN1("[setupNetwork()] APNAME is not defined");
  // use ESP + ChipID for the name of the AP
  wifiManager.autoConnect();
#endif

  DEBUGPRINTLN0("[setupNetwork()] WIFI:");
  DEBUGPRINT0("[setupNetwork()] IP: ");
  DEBUGPRINT0(WiFi.localIP());
}