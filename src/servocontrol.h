#include <DebugUtils/DebugUtils.h>
#include <Servo.h>

// PROTOTYPES
void setupServo();
void moveHome();
// !PROTOTYPES

// VARIABLES
Servo wrench;
// !VARIABLES

/**
 * This function initializes the usage of the servo.
 * It sets the pin for the endstop at the top and the pin for the servo.
 * After the assignments of the pin the position of the sphere has to be
 * resetted so that the program can work with a clean position.
 * It gets called only once during the method setup.
 * @returns null
 */
void setupServo()
{
  DEBUGPRINTLN0("[setupServo()]");
  pinMode(STOPPIN, INPUT);
  wrench.attach(SERVOPIN);
  DEBUGPRINTLN1("[setupServo()] Servo attached to " + SERVOPIN);
  moveHome();
}

/**
 * This function is here to reset the position of the sphere.
 * This can be necessary at certain points in the program. (e.g.: during startup)
 * First the sphere moves up with the selected speed until it reaches the endstop.
 * Then it moves down until the endstop is no more pressed and stops.
 * @returns null 
 */
void moveHome()
{
  DEBUGPRINTLN0("[moveHome()]");
  DEBUGPRINTLN1("[moveHome()] moving up");
  while (digitalRead(STOPPIN) == LOW)
  {
    yield();                  // overcome blocking code and a wdt reset
    wrench.write(STOCKSPEED); // TODO: introduce speed selected by user!
  }
  DEBUGPRINTLN1("[moveHome()] moving down now");
  do
  {
    yield(); // overcome blocking code and a wdt reset
    wrench.write(OFFSETSPEED);
  } while (digitalRead(STOPPIN) == HIGH);
  wrench.write(STOPSPEED);
  DEBUGPRINTLN1("[moveHome()] stopping");
}